# SRIR WebSocket Python :panda_face: 
---

## Schemat połącznia
Odpalany jest serwer i klient.

Serwer czeka na wiadomość

Klient wysyła informacje o ilości lini kodu (string, np. "4")

Serwer przygotowuje sie na odebranie okreslonej ilosci lini (stringi)

Klient wysyła kolejno linie kodu (1 linia = 1 string)

Klient czeka na odebranie 3 wiadomosci zwrotnych (1 dla informacji o kompilacji, 2 dla wynik działania/wykonania kodu, 3 analiza kodu i feedback)

Serwer przesyła powyższe dane

Rozłączenie klienta (serwer czeka na kolejną wiadomość z punktu 2 czyli ilość linii)

## Trello
` https://bitbucket.org/Biggamaze/srir_python_websocket/addon/trello/trello-board `

To clone this repo run command:

`git clone https://<nazwa_uzytkownika>@bitbucket.org/Biggamaze/srir_python_websocket.git`

## Git stuff

Gałąź master jest zablokowana - merge tylko przez pull requesty, ale nikt ich nie musi sprawdzać.
To jest po to, że jakby dwie osoby pracowały i pushowały na raz, żeby nie sypało błędami

## Example Installation:

`pip install websockets`

`python main.py`

`open index.html in browser`
### Client
* **Kamil** - Przesłanie z jakiegoś komputera K1 na inny komputer K2 kodu źródłowego
programu P (dowolnego, ale w tym samym języku programowania** w którym jest
napisany projekt).
### Server
* **Radek** - Sprawdzenie poprawności przesłanego kodu źródłowego P na komputerze
K2 i dostarczenie informacji zwrotnej na komputer K1. Jeżeli program może zostać
skompilowany to kompilacja tego programu.
* **Maria** - Wykonanie programu P na komputerze K2 oraz przesłanie informacji
zwróconej przez program P na komputer K1.
* **Marek** - Składowanie i porównywanie kolejnych przesłanych programów na
komputer
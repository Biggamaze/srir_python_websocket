import asyncio
import websockets

async def hello():
    async with websockets.connect(
            'ws://localhost:3000') as websocket:
        input = "2+2"

        await websocket.send(input)
        print(f"> {input}")

        greeting = await websocket.recv()
        print(f"< {greeting}")

        greeting = await websocket.recv()
        print(f"< {greeting}")

asyncio.get_event_loop().run_until_complete(hello())
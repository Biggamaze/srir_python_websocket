class PylintOutput(object):
    exceptions = []
    warnings = []
    comments = []
    others = []

    def write(self, line: str):
        if line.startswith("C:"):
            self.comments.append(line.replace("C:", "Comment:", 1))
        elif line.startswith("W:"):
            self.warnings.append(line.replace("W:", "Warning:", 1))
        elif line.startswith("E:"):
            self.exceptions.append(line.replace("E:", "Exception:", 1))
        else:
            self.others.append(line)

    def read(self):
        return


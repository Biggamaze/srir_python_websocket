import Server.src.execution as execution
from unittest import TestCase


class TestExecution(TestCase):
    def testCorrectExecution(self):
        path = "test_files_for_execution/correct_test_file.py"
        result = execution.execute(path)
        self.assertTrue(result == 2)

    def testExecutionWithRuntimeError(self):
        path = "test_files_for_execution/incorrect_test_file.py"
        result = execution.execute(path)
        self.assertTrue("Runtime Error" in result)

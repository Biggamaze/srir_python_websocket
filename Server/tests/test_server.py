import uuid
from Server.lib.pylint_output import PylintOutput
import Server.src.server as server
import IntegrationTesting.helpers.output_helpers as oh


def test_format_compilation_output_full_output_is_concatenated():
    output = PylintOutput()
    output.exceptions = ["Amarena", "Django", "DDD"]
    output.warnings = ["Nanna", "Anna"]
    output.comments = ["No comment"]
    output.others = ["\n", "\n", "o", "o"]

    formatted = server.format_compilation_output(output)

    assert isinstance(formatted, list)
    assert set(formatted) == set(["Amarena", "Django", "DDD", "Nanna", "Anna", "No comment", "\n", "o"])


def test_process_received_code_valid_code_given_no_exceptions():
    code = "def add(a, b): \
                    return 2 + 2"
    guid = uuid.uuid4()

    server.process_received_code(code, guid)


def test_process_received_code_invalid_code_exception_found(capsys):
    code = "def add(): \
                    return a ^&^% b"
    guid = uuid.uuid4()

    for i in server.process_received_code(code, guid):
        print(f"--{i}")

    out, err = capsys.readouterr()
    _, server_out, _, _ = oh.split_output(out, err)

    assert "exception during compilation" in server_out
    assert "--Not executed" in out
    assert "--No feedback" in out



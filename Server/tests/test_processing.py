import Server.src.processing as proc
from unittest import TestCase
import uuid

class TestProcessing(TestCase):

    def test_compile_code__correct_method_as_input__no_error(self):
        code = "def add(a, b): \
                        return a + b"

        guid = uuid.uuid4()
        res, filename = proc.compile_code(code, guid)

        self.assertTrue(res.exceptions.__len__() == 0)

    def test_compile_code__simple_method_as_input__no_error(self):
        code = "def add(a, b): \
                        return 2 + 2"

        guid = uuid.uuid4()
        res, filename = proc.compile_code(code, guid)

        self.assertTrue(res.exceptions.__len__() == 0)

    def test_compile_code_incorrect_method_as_input__exceptions_found(self):
        code = "def add(): \
                        return a ^&^% b"

        guid = uuid.uuid4()

        res, filename = proc.compile_code(code, guid)

        self.assertTrue(res.exceptions.__len__() > 0)

    def test_compile_code_incorrect_parameters_as_input__exceptions_found(self):
        code = "def add(): \
                        return a + b"

        guid = uuid.uuid4()
        res, filename = proc.compile_code(code, guid)

        self.assertTrue(res.exceptions.__len__() > 0)

    def test_compile_code_some_signs_as_input__exceptions_found(self):
        code = "sadasld88899aaaa#$%"

        guid = uuid.uuid4()
        res, filename = proc.compile_code(code, guid)

        self.assertTrue(res.exceptions.__len__() > 0)
import Server.src.storing as storing
from unittest import TestCase
import sqlite3

class TestExecution(TestCase):
    def testWordsNumber(self):
        path = "test_files_for_storing/test_file_analize_words.py"
        result = storing.analize_words_num(path)
        self.assertTrue(result == 11)

    def testLinesNumber1(self):
        path = "test_files_for_storing/test_file_analize_lines.py"
        result = storing.analize_num_lines(path)
        self.assertTrue(result == 3)

    def testLinesNumber2(self):
        conn = sqlite3.connect("db/srir.db")
        c = conn.cursor()
        result = storing.prepare_analize_output(c, 1, 1)
        self.assertTrue("ANALIZA" in result)

"""@package docstring
Moduł analizujący poprawność kodu

"""

from pylint import lint
from pylint.reporters.text import TextReporter
from Server.lib.pylint_output import PylintOutput
import os


def compile_code(response: str, guid) -> (PylintOutput, str):
    """Documentation for function

        Odbiera kod a następnie zapisuje do pliku do analizy poprawności.
    """
    filename = f'codes/{str(guid)}.py'
    pylint_output = PylintOutput()
    try:
        file = open(filename, "w")
        file.write(response)
        file.close()
        pylint_output = run_pylint(filename)
    except Exception as e:
        pylint_output.exceptions = [str(e)]
        pylint_output.warnings = []
        pylint_output.comments = []
        os.remove(filename)

    return pylint_output, filename


def run_pylint(filename: str) -> PylintOutput:
    """Documentation for function

        Analizuje poprawność kodu używając PyLint. Zwraca wynik analizy w postaci klasy.
    """
    pylint_output = PylintOutput()

    lint.Run([filename], reporter=TextReporter(pylint_output), exit=False)

    return pylint_output



"""@package docstring
Moduł serwera

Działanie:
Serwer startuje w event loopie. Event loop sprawdza czy przyszły nowe zapytania na serwer.
Następnie odbiera ilość linii od serwera, a potem cały kod do wykonania.
Poprawność kodu jest następnie sprawdzana PyLintem, a potem wykonana.
Na końcu następuje analiza kodu.
Z każdego etapu jest zwracan do klienta informacja o stanie procesu.
"""

from concurrent.futures import ThreadPoolExecutor
import Server.src.processing as comm
import Server.src.execution as exc
import Server.src.storing as stor
import asyncio
import websockets
import uuid
import json


async def server(websocket, path):
    """Documentation for function

        Funcja główna serwera. Obsługuje żądania i wywołuje odpowiednie funkcje.
    """
    guid = uuid.uuid4()
    try:
        code = await receive_until_completed(websocket)
        print(f"S:{guid}:received complete code")
    except websockets.exceptions.ConnectionClosed as ec:
        print(f"S:{guid}:exception during receiving code - {str(ec.args[0])}")
        return
    except Exception as e:
        msg = f"S:{guid}:exception during receiving code - {str(e.args[0])}"
        print(msg)
        await websocket.send(msg)
        return

    for output in process_received_code(code, guid):
        await websocket.send(str(output))


async def receive_until_completed(websocket):
    """Documentation for function

        Funkcja odbiera numer linii,a następnie nieblokująco odbiera kolejne linie kodu. W razie timeoutu rzuca wyjątkiem.
    """
    num_lines = await websocket.recv()

    if not num_lines or not isinstance(int(num_lines), int):
        raise Exception("Please provide correct number of lines")

    code = ""

    try:
        for t in range(int(num_lines)):
            line = await asyncio.wait_for(websocket.recv(), timeout=5)  # supposedly this operation is expensive :/
            code = ''.join((code, line))
    except asyncio.TimeoutError:
        raise Exception("Receiving operation timed out. Please check if correct number of lines is provided")

    return code


def process_received_code(code, guid):
    """Documentation for function

        Funkcja kombajn - sprawdza kod, potem go wykonuje, a następnie analizuje i składuje w bazie.
    :param code: Kod programu do wykonania
    :param guid: GUID służący do identyfikacji wątku.
    """
    print(f"S:{guid}:compiling code")
    comp_output, filename = comm.compile_code(code, guid)
    print(f"S:{guid}:compiled code")
    print(f"Filename: {filename}")
    yield '\n'.join([str(x) for x in format_compilation_output(comp_output)])

    if not comp_output.exceptions:
        print(f"S:{guid}:compilation successful ... executing ...")
        execution_output = exc.execute(filename)
        yield execution_output
        print(f"S:{guid}:execution successful")

        print(f"S:{guid}:storing code")
        store_output = stor.store(filename, comp_output, execution_output)
        print(f"S:{guid}:stored")
        yield store_output
        print(f"S:{guid}:server job finished")
        return

    print(f"S:{guid}:exception during compilation - {comp_output.exceptions}")

    yield "Not executed"
    yield "No feedback"


def format_compilation_output(compilation_output):
    """Documentation for function

        Funkcja formatuje wartości zwracane przez analizator kodu.
    """
    others = list(set(compilation_output.others))
    return compilation_output.exceptions + compilation_output.warnings + compilation_output.comments + others


def init(loop_in):
    """Documentation for function

        Inicjalizuje serwer
    """
    asyncio.set_event_loop(loop_in)

    global loop
    loop = loop_in

    with open("config.json") as f:
        conf_data = json.load(f)

    start_server = websockets.serve(server, conf_data["ip"], conf_data["port"])
    global executor
    executor = ThreadPoolExecutor(max_workers=5)

    loop.run_until_complete(start_server)
    loop.run_forever()


def terminate(loop_in):
    """Documentation for function

        Kończy pracę serwera.
    """
    loop.call_soon_threadsafe(loop_in.stop())


if __name__ == '__main__':
    """Documenttion for function
    
        Punkt wejściowy do programu. 
    """
    loop = asyncio.get_event_loop()
    init(loop)

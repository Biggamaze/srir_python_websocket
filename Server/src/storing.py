"""@package docstring
Moduł analizy i przechowywania.
"""

import time
import datetime
import sqlite3
import re


def store(code_filename: str, compilation_output, execution_output):
    """Documentation for a function.

       Funkcja wykonuje funkcja ta tworzy bazę danych i zarządza wpisami do niej ".
    """

    path = 'db/srir.db'
    conn = sqlite3.connect(path)
    c = conn.cursor()
    create_table(c, conn)
    create_analysistable(c, conn)

    dynamic_data_entry(code_filename, conn, c, execution_output, compilation_output)
    num_lines = analize_num_lines(code_filename)
    num_words = analize_words_num(code_filename)
    fillanalisis(conn, c, num_words, num_lines, code_filename.split(".")[0].split("/")[1])
    result = prepare_analize_output(c, num_lines, num_words)
    conn.close()
    return result


def create_table(c, conn):
    """Documentation for a function.

       Funkcja wykonuje tworzy tabele srir w bazie danych jeżeli ona nie istnieje ".
    """
    c.execute(
        "CREATE TABLE IF NOT EXISTS "
        "srir(unix REAL, datestamp TEXT, keyword TEXT, programcode TEXT, execution_output TEXT)")
    conn.commit()


def create_analysistable(c, conn):
    """Documentation for a function.

       Funkcja wykonuje tworzy tabele analysis w bazie danych jeżeli ona nie istnieje ".
    """
    c.execute(
        "CREATE TABLE IF NOT EXISTS "
        "analysis(guid TEXT, num_words INTEGER, num_lines INTEGER)")
    conn.commit()


def dynamic_data_entry(file_name, conn, c, execution_output, compilation_output):
    """Documentation for a function.

       Funkcja wpisuje dane do tabeli w bazie SQLite3 ".
    """
    for item in compilation_output.exceptions:
        c.execute("INSERT INTO srir(item) VALUES(?)", (item))
    conn.commit()
    unix = int(time.time())
    date = str(datetime.datetime.fromtimestamp(unix).strftime('%Y-%m-%d %H:%M:%S'))
    keyword = 'Python SRIR project'

    c.execute("INSERT INTO srir(unix, datestamp, keyword, programcode, execution_output) VALUES (?, ?, ?, ?, ?)",
              (unix, date, keyword, file_name, execution_output))

    conn.commit()


def fillanalisis(conn, c, num_words, num_lines, guid):
    """Documentation for a function.

       Funkcja wpisuje dane do tabeli analysis w bazie SQLite3 ".
    """
    c.execute("INSERT INTO analysis(guid ,num_words, num_lines) VALUES (?,?,?)",
              (guid, num_words, num_lines))
    conn.commit()


def analize_words_num(file_path):
    """Documentation for a function.

       Funkcja wykonuje kod Pythona znajdujący się w pliku. Jako argument przyjmuje ścieżkę do pliku zawierającego kod. Zwracany jest wynik wykonanego kodu, lub w przypadku wystąpienia Runtime Error informcja, jaki błąd wystąpił".
    """
    file = open(file_path, "r")
    code = file.read()
    file.close()
    words_array = re.split('\s', code)
    num_words = words_array.__len__() - 1

    return num_words


def analize_num_lines(file_path):
    """Documentation for a function.

       Funkcja liczy ilośc linii kodu oraz zwraca ją ".
    """
    file = open(file_path, "r")
    code = file.read()
    file.close()
    lines_array = re.split('\n', code)
    lines_num = lines_array.__len__() - 1
    print(lines_num)

    return lines_num


# dodac zapisywanie tych danych do bazy


def prepare_analize_output(c, lines_num, num_words):
    """Documentation for a function.

       Funkcja liczy porównuje wyniki analizy z zawartościa bazy danych ".
    """
    try:
        result = "ANALIZA\n"
        c.execute(f'SELECT * FROM  analysis WHERE num_lines > {lines_num}')
        more_lines_in = len(c.fetchall())
        result += "więcej lini zawierało:\t" + str(more_lines_in) + "\tplików z kodem\n"
        c.execute(f'SELECT * FROM  analysis WHERE num_lines < {lines_num}')
        less_lines_in = len(c.fetchall())
        result += "mniej lini zawierało:\t" + str(less_lines_in) + "\tplików z kodem\n"
        c.execute(f'SELECT * FROM  analysis WHERE num_lines = {lines_num}')
        same_lines_in = len(c.fetchall())
        result += "tyle samo lini zawierało:\t" + str(same_lines_in) + "\tplików z kodem\n"

        c.execute(f'SELECT COUNT(*) FROM  analysis WHERE num_words > {num_words}')
        more_words_in = len(c.fetchall())
        result += "więcej słów zawierało:\t" + str( more_words_in) + "\tplików z kodem\n"
        c.execute(f'SELECT * FROM  analysis WHERE num_words < {num_words}')
        less_words_in = len(c.fetchall())
        result += "mniej słów zawierało:\t" + str(less_words_in) + "\tplików z kodem\n"
        c.execute(f'SELECT * FROM  analysis WHERE num_words = {num_words}')
        same_words_in = len(c.fetchall())
        result += "tyle samo słów zawierało:\t" + str(same_words_in) + "\tplików z kodem\n"
    except Exception as e:
        print(f"{e.args[0]},{e.args[1]}")

    return result

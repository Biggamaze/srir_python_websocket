"""@package docstring
Moduł wykonania.
"""


def execute(file_path):
    """Documentation for a function.

       Funkcja wykonuje kod Pythona znajdujący się w pliku. Jako argument przyjmuje ścieżkę do pliku zawierającego kod. Zwracany jest wynik wykonanego kodu, lub w przypadku wystąpienia Runtime Error informcja, jaki błąd wystąpił".
    """
    file = open(file_path, "r")
    code = file.read()
    file.close()
    try:
        result = eval(compile(code, '<string>', 'eval'))
    except Exception as e:
        result = f"Runtime Error: {str(e)}"
    return result

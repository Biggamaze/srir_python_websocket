from unittest import TestCase

from Client.src.client_logic import loadFile


class TestLoadFile(TestCase):
    def test_loadFile(self):
        self.assertEqual(loadFile('test.txt'),['abc'])

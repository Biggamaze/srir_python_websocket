from unittest import TestCase

from Client.src.client_logic import client_run


class TestClient_run(TestCase):
    def test_client_run(self):
        self.assertEqual(client_run('ws://127.0.0.1:8001/', ["abc", "cba"]), "done")
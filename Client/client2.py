"""@package docstring
Moduł klienta.
Instalacja:
`open terminal`
`pip install -r requirements.txt` or `pip install websockets`
`python client2.py`

Działanie (klienta):
Na początku wprwadza się adres (ip i port) serwera np. 'ws://127.0.0.1:3000/', dalej wybiera się plik z rozszerzeniem *.py (Python) do wykonania. Ostatnim krokiem jest naciśnięcie przycisku 'Send Code' który wysyła wybrany plik do wykonania do serwera (przez websocket), wyniki działania serwera wyświetlone zostaną w strefeie oznacoznej jako 'Output:'

Repozytorium:
https://bitbucket.org/Biggamaze/srir_python_websocket/src/kamil/Client/
"""

# Websocket Client App
from tkinter import *
from tkinter import filedialog
import asyncio
import websockets
import random
import uuid

# global variables
data = ""
lines = 1
result = ["null", "null", "null"]


# s_adress="ws://127.0.0.1:3000/"

def loadFile(local):
    """Documentation for a function.

       Funkcja odpalana po wybraniu pliku do wykonania którego lokalizacja przechowywana jest w zmiennej local. Jej zadaniem jest odpowiednie wczytanie (do zmiennej x) kodu oraz (do zmiennej lines) ilości linii kodu.
    """
    fo = open(local, 'r')
    x = fo.readlines()
    global lines
    lines = sum(1 for line in open(local))
    fo.close()
    return x


async def client(address, data):
    """Documentation for a function.

       Główna funkcja programu. Jako argument przyjumje adress i port serwera z którym ma się połączyć. Na początku wyysła ilość lini kodu, potem sam kod (ile lini, tyle wysłanych komunikatów). Na koniec do 3 elementowej tablicy (result) zbiera od serwera kolejno informacje o procesie komilacji, wyniku dziłania programu i informacji zwrotnej z analizą.
    """
    guid = uuid.uuid4()
    async with websockets.connect(address) as websocket:
        print(f"C:{guid}:sending number of lines")
        global lines
        await websocket.send(str(lines))

        print(f"C:{guid}:sending data")
        for t in range(lines):
            await websocket.send(str(data[t]))

        print(f"C:{guid}:awaiting for result")
        global result

        for t in range(3):
            result[t] = await websocket.recv()
            result[t] = result[t] + "\n"
            print(f"C:{guid}:{result[t]}")

        print(f"C:{guid}:got result")
        await asyncio.sleep(random.random() * 1)


def client_run(address, data, loop_in=None):
    """Documentation for a function.

       Jako argument przyjmuje adress serwera i kod programu do wykonania. Jej zadaniem jest odpalenie kodu z metody client() i zwrócenie wyniku.
    """
    global result
    if not loop_in:
        loop = asyncio.get_event_loop()
    else:
        loop = loop_in

    loop.run_until_complete(client(address, data))
    return result


def click_select():
    """Documentation for a function.

       Funkcja obsługi przycisku 'Select file..', otwiera okno systemowe pozwalajace wybrać plik *.py do wykonania i zwraca jego lokalizacje.
    """
    print("selecting file")
    window.fileName = filedialog.askopenfilename(filetypes=(("Python files", "*.py"), ("All files", "*.*")))
    global data
    data = loadFile(window.fileName)
    print(data)


def click_run():
    """Documentation for a function.

       Funkcja powiązana z przyciskiem 'Send code', pobiera adres serwera wpisany w GUI, odpala pozostałe funkcje, celem uzykania wyniku
    """
    print("running")
    address = text_entry.get()
    try:
        client_run(address, data)
    except:
        output.insert('1.0', "\n\nError, Couldn't connect to server")
    update_output(result)


def update_output(result):
    """Documentation for a function.

       Wyswietlna wyniki (z tablicy result) do wyznaczonego w GUI obszaru razem z formatowaniem.
    """
    print("printing result")
    output.insert('1.0',
                  ("Compilation:\n" + result[0] + '\n' + "Execution:\n" + result[1] + '\n'"Feedback:\n" + result[2]))


def click_loadConfig():
    """Documentation for a function.

       Funkcja powiązana z przyciskiem 'Load config', wczytuje ostatnio zapisaną konfiguracje klienta czyli adres serwera i port
    """
    print("loading")
    fo = open('client_config.txt', 'r')
    x = fo.readlines()
    fo.close()
    text_entry.insert(0, x[0])


def click_saveConfig():
    """Documentation for a function.

       Funkcja powiązana z przyciskiem 'Save config', zapisuje konfiguracje klienta czyli adres serwera i port
    """
    print("saving")
    fo = open('client_config.txt', 'w')
    x = text_entry.get()
    fo.write(x)
    fo.close()


if __name__ == '__main__':
    # main
    window = Tk()
    window.title("Client App")
    window.configure(background="white")
    window.resizable(width=False, height=False)

    # image
    image = PhotoImage(file="img/logo.png")
    Label(window, image=image, bg="white").grid(row=0, column=0, columnspan=2, sticky=N)

    # window icon
    icon = PhotoImage(file="img/icon.png")
    window.tk.call('wm', 'iconphoto', window._w, icon)

    # label connection
    Label(window, text="Server address:", bg="white", fg="gray", font="none 12").grid(row=1, column=0, sticky=W)

    # textbox server address
    text_entry = Entry(window, text="s", width=31, bg="white")
    text_entry.grid(row=1, column=1, sticky=W)
    # text_entry.insert(0, s_adress)

    # button Save config
    button_sel = Button(window, text="Save config", width=20, command=click_saveConfig)
    button_sel.grid(row=2, column=0, sticky=W)

    # button Load config
    button_sel = Button(window, text="Load config", width=20, command=click_loadConfig)
    button_sel.grid(row=2, column=1, sticky=E)

    # button select py file
    button_sel = Button(window, text="Select file..", width=48, command=click_select)
    button_sel.grid(row=3, column=0, columnspan=2, sticky=N)

    # button send code to server
    button_sen = Button(window, text="Send code", width=48, command=click_run)
    button_sen.grid(row=4, column=0, columnspan=2, sticky=N)

    # text box - output
    output = Text(window, width=42, height=30, wrap=WORD, background="white")
    output.grid(row=0, column=2, rowspan=4, sticky=W)

    # label output
    Label(window, text="Server response..", bg="white", fg="gray", font="none 12").grid(row=4, column=2, sticky=N)

    # run main loop
    window.mainloop()

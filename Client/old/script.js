const execButtton=document.getElementById("exec");

function getCode(){
 let code=document.querySelector(".ace_content").innerText;
 return code;
}

function setOutput(output){
 let cons=document.querySelector(".sh.sh-return");
 cons.innerHTML=output;
}

execButtton.addEventListener("click",()=>{
    let code=getCode();

    //temporary
    //alert(code);

    setOutput(code);

});
let exampleSocket = new WebSocket("ws://www.example.com/socketserver", "protocolOne");

//const socket = new WebSocket("ws://127.0.0.1:5000/"),
//messages = document.createElement('ul');

// socket.onmessage = function (event) {
// let messages = document.getElementsByTagName('ul')[0],
//     message = document.createElement('li'),
//     content = document.createTextNode(event.data);
// message.appendChild(content);
// messages.appendChild(message);
// };
// document.body.appendChild(messages);

socket.onopen = function (event) {
    socket.send("Hello there"); 
  };

socket.onmessage = function(event) {
    let msg = JSON.parse(event.data);
    let time = new Date(msg.date);
    let timeStr = time.toLocaleTimeString();
    
    let text=timeStr+"<br>"+msg;
    
    if (text.length) 
        setOutput(text);
  };
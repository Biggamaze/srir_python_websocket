# SRIR WebSocket Python :panda_face:
---

To clone this repo run command:

`git clone https://Biggamaze@bitbucket.org/Biggamaze/srir_python_websocket.git`

Installation:

`pip install websockets`

`python main.py`

`open index.html in browser`

**Happy coding!**

# Module 1 - Kamil
![Image](https://bitbucket.org/Biggamaze/srir_python_websocket/raw/4253b8616efd560bfad11e9e49b5cd33148dd85a/frontend/screen.PNG)

# Socket default address and port 
`127.0.0.1:5000`
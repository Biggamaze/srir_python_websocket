"""@package docstring
Moduł klienta.


Repozytorium: https://bitbucket.org/Biggamaze/srir_python_websocket/src/kamil/Client/
"""
import asyncio
import websockets
import random

result="client error"
lines=""

def loadFile(local):
    """Documentation for a function.

       More details.
    """
    fo = open(local, 'r')
    x = fo.readlines()
    global lines
    lines=sum(1 for line in open(local))
    fo.close()
    return x

async def client(address, data):
    """Documentation for a function.

       More details.
    """
    async with websockets.connect(address) as websocket:
        await websocket.send(str(lines))

        for t in range(lines):
            await websocket.send(str(data[t]))
            await asyncio.sleep(random.random() * 1)

        global result
        result = await websocket.recv()
        print(result)

        await asyncio.sleep(random.random() * 1)

def client_run(address, data):
    """Documentation for a function.

       More details.
    """
    global result
    asyncio.get_event_loop().run_until_complete(client(address, data))
    return result
import asyncio
import logging
import ssl

import websockets


# logging.basicConfig(level=logging.DEBUG)

async def hello(websocket, path):
    lines="1"
    lines = await websocket.recv()
    lines2=int(lines)
    for t in range(lines2):
        name = await websocket.recv()
        print("< {}".format(name))

    # greeting = "Hello {}!".format(name)
    await websocket.send("done")
    await websocket.send("done")
    await websocket.send("done")

    # print("> {}".format(greeting))
    # print("done")

# context = ssl.create_default_context(ssl.Purpose.CLIENT_AUTH)
# context.load_cert_chain(certfile='websockets/testcert.pem')
# context.set_ciphers('RSA')

start_server = websockets.serve(hello, '127.0.0.1', 8001)

asyncio.get_event_loop().run_until_complete(start_server)
asyncio.get_event_loop().run_forever()
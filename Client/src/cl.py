import asyncio
import websockets
import random
def readCode():
    fo = open('sio.py', 'r')
    x = fo.readlines()
    # print(x)
    fo.close()
    return x
# readCode()

async def test():
    async with websockets.connect('ws://localhost:8001') as websocket:
        # await websocket.send(str(readCode()[0:2]))
        # response = await websocket.recv()
        # print(response)
        for t in range(8):
            await websocket.send(str(readCode()[t]))
            await asyncio.sleep(random.random() * 1)

        response = await websocket.recv()
        print(response)
        await asyncio.sleep(random.random() * 1)


asyncio.get_event_loop().run_until_complete(test())
# asyncio.get_event_loop().run_forever()
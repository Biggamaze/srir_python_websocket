import logging
import websockets
import asyncio

def logger():
    logger = logging.getLogger('websockets')
    logger.setLevel(logging.INFO)
    logger.addHandler(logging.StreamHandler())

def ws_connect(address):
    print("Connecting to => "+address)
    asyncio.get_event_loop().run_until_complete(hello())

def setOutput(output):
    return output

def openFile(path):
    file=open(path,"rb") #powinno byc binary jak nie to r to string
    data=file.read()
    file.close()
    return data



@asyncio.coroutine
def hello():
    websocket = yield from websockets.connect('ws://localhost:8765/')

    try:
        #zrob testy na tym czy dziala
        name = input("What's your name? ")
        yield from websocket.send(name)
        print("> {}".format(name))

        greeting = yield from websocket.recv()
        print("< {}".format(greeting))

#senfing data

##reciving output - lepiej dac merge tego do glownego pliku  i podmiecnic text area jak przyjdzie
        output=yield from websocket.recv()
        setOutput(output)


    finally:
        yield from websocket.close()


#pingowanie
while True:
    try:
        msg = await asyncio.wait_for(ws.recv(), timeout=20)
    except asyncio.TimeoutError:
        # No data in 20 seconds, check the connection.
        try:
            pong_waiter = await ws.ping()
            await asyncio.wait_for(pong_waiter, timeout=10)
        except asyncio.TimeoutError:
            # No response to ping in 10 seconds, disconnect.
            break
    else:
        # do something with msg
        ...
# Websocket Client App
from tkinter import *
from tkinter import filedialog

# local imports
from Client.src.client_logic import loadFile
from Client.src.client_logic import client_run

# global variables
data = ""


def click_select():
    print("selecting file")
    window.fileName = filedialog.askopenfilename(filetypes=(("Python files", "*.py"), ("All files", "*.*")))
    global data
    data = loadFile(window.fileName)
    print(data)


def click_run():
    print("running")
    address = text_entry.get()
    result = client_run(address, data)
    update_output(result)


def update_output(result):
    output.insert('1.0', (result + '\n'))


# main
window = Tk()
window.title("Client App")
window.configure(background="white")

# image
image = PhotoImage(file="img/logo.png")
Label(window, image=image, bg="white").grid(row=0, column=0, columnspan=3, sticky=N)

# window icon
icon = PhotoImage(file="img/icon.png")
window.tk.call('wm', 'iconphoto', window._w, icon)

# label connection
Label(window, text="Server address:", bg="white", fg="gray", font="none 12").grid(row=1, column=0, sticky=W)

# textbox server address
text_entry = Entry(window, text="s", width=36, bg="white")
text_entry.grid(row=1, column=1, sticky=W)
text_entry.insert(0, "ws://127.0.0.1:3000/")

# button connect to server
# button_con=Button(window,text="Connect", width=7,command=click_connect)
# button_con.grid(row=1, column=2)

# button select py file
button_sel = Button(window, text="Select file..", width=47, command=click_select)
button_sel.grid(row=2, column=0, columnspan=3, sticky=N)

# button send code to server
button_sen = Button(window, text="Run Code", width=47, command=click_run)
button_sen.grid(row=3, column=0, columnspan=3, sticky=S)

# label output
Label(window, text="Output:", bg="white", fg="gray", font="none 12").grid(row=4, column=0, sticky=W)

# text box - output
output = Text(window, width=42, height=15, wrap=WORD, background="white")
output.grid(row=5, column=0, columnspan=3, sticky=W)

# run main loop
window.mainloop()

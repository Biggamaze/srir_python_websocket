from unittest import TestCase

from Client.client2 import loadFile


class TestLoadFile(TestCase):
    def test_loadFile(self):
        self.assertEqual(loadFile('test.txt'), ['abc'])

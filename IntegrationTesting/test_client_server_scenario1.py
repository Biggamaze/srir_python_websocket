import pytest
import IntegrationTesting.helpers.runners as r
import IntegrationTesting.helpers.output_helpers as oh
import IntegrationTesting.helpers.assert_helpers as ass
import os
import sys
import sqlite3

server_out = []
client_out = []
error = []
filename = ""

@pytest.mark.order1
def test_integration__can_execute_simple_code__send_code__stage1(capsys):
    r.run_server()
    r.run_client_in_this_thread("ws://127.0.0.1:3000/", "samplecodes/scenario1.py")

    out, err = capsys.readouterr()
    global client_out, server_out, error, filename
    client_out, server_out, error, filename = oh.split_output(out, err)

    r.terminate_server()


@pytest.mark.order2
def test_integration__can_execute_simple_code__check_output__stage2():
    ass.assert_client_logs_complete(client_out)
    ass.assert_calculation_result_is(client_out, "None", "4")


@pytest.mark.order3
def test_integration__can_execute_simple_code__check_output__stage3():
    ass.assert_server_succesfull_logs_complete(server_out)


@pytest.mark.order4
def test_integration__can_execute_simple_code__check_error_output__stage4():
    assert error[0] == "No config file found, using default configuration"


@pytest.mark.order5
def test_integration__can_execute_simple_code__check_db__stage5(capsys):
    conn = sqlite3.connect("db/srir.db")
    c = conn.cursor()

    c.execute("SELECT num_words, num_lines FROM analysis WHERE guid = (?)", (filename.split(".")[0].split("/")[1],))
    data = c.fetchone()

    conn.close()

    assert data[0] == 3
    assert data[1] == 1


@pytest.mark.order6
def test_clean_up():
    folder = "codes"
    for file in os.listdir(folder):
        filepath = os.path.join(folder, file)
        if os.path.isfile(filepath):
            os.unlink(filepath)

    conn = sqlite3.connect("db/srir.db")
    c = conn.cursor()

    c.execute("DELETE FROM srir;")
    c.execute("DELETE FROM analysis;")
    conn.commit()
    conn.close()

    sys.stdout.flush()



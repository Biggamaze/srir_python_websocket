import pytest
import IntegrationTesting.helpers.runners as r
import IntegrationTesting.helpers.output_helpers as oh
import IntegrationTesting.helpers.assert_helpers as ass
import os
import sys
import sqlite3

feedback_bad = "No feedback"
execution_out_bad = "Not executed"
feedback_good = "None"
execution_out_good = "4"
server_address = "ws://127.0.0.1:3000/"
bad_code_to_execute = "samplecodes/samplecode2.txt"
good_code_to_execute = "samplecodes/scenario1.py"

server_out = {}
client_out = {}
error = []


@pytest.mark.order1
def test_integration_can_handle_mixed_code_run(capsys):
    r.run_server()
    client_tasks = []
    for i in range(50):
        if i % 2 == 0:
            client_tasks.append(r.create_client_task(server_address, bad_code_to_execute))
        else:
            client_tasks.append(r.create_client_task(server_address, good_code_to_execute))

    for task in client_tasks:
        task.start()

    for task in client_tasks:
        task.join()

    r.terminate_server()

    out, err = capsys.readouterr()
    global client_out, server_out, error
    client_out = oh.split_multi_instance_output(out, "C:")
    server_out = oh.split_multi_instance_output(out, "S:")


@pytest.mark.order2
def test_integration_can_handle_mixed_code_check_client_output():
    for key in client_out.keys():
        ass.assert_client_logs_complete(client_out[key])
        ass.assert_calculation_result_is_either_or(client_out[key], feedback_bad, feedback_good,
                                                   execution_out_bad, execution_out_good)


@pytest.mark.order3
def test_integration_can_handle_mixed_code_check_server_output():
    for key in server_out.keys():
        ass.assert_server_completely_positive_or_negative_compilation(server_out[key])


@pytest.mark.order4
def test_clean_up():
    folder = "codes"
    for file in os.listdir(folder):
        filepath = os.path.join(folder, file)
        if os.path.isfile(filepath):
            os.unlink(filepath)

    conn = sqlite3.connect("db/srir.db")
    c = conn.cursor()

    c.execute("DELETE FROM srir;")
    c.execute("DELETE FROM analysis;")
    conn.commit()
    conn.close()

    sys.stdout.flush()

import pytest
import IntegrationTesting.helpers.runners as r
import IntegrationTesting.helpers.output_helpers as oh
import IntegrationTesting.helpers.assert_helpers as ass
import os
import sys
import sqlite3

client_out = {}
feedback = "None"
execution_out = "4"
server_address = "ws://127.0.0.1:3000/"
code_to_execute = "samplecodes/scenario1.py"


@pytest.mark.order0
def test_integration_initialize():
    r.run_server()
    client_tasks = []
    for _ in range(50):
        client_tasks.append(r.create_client_task(server_address, code_to_execute))

    for task in client_tasks:
        task.start()

    for task in client_tasks:
        task.join()


@pytest.mark.order1
def test_integration_can_handle_2_clients__run_no_exceptions(capsys):
    client_tasks = []
    for _ in range(2):
        client_tasks.append(r.create_client_task(server_address, code_to_execute))

    for task in client_tasks:
        task.start()

    for task in client_tasks:
        task.join()

    out, _ = capsys.readouterr()
    global client_out
    client_out = oh.split_multi_instance_output(out, "C:")


@pytest.mark.order2
def test_integration_can_handle_2_clients__output_correct():
    for key in client_out.keys():
        ass.assert_client_logs_complete(client_out[key])
        ass.assert_calculation_result_is(client_out[key], feedback, execution_out)

    sys.stdout.flush()


@pytest.mark.order3
def test_integration_can_handle_20_clients__run_no_exceptions(capsys):
    client_tasks = []
    for _ in range(20):
        client_tasks.append(r.create_client_task(server_address, code_to_execute))

    for task in client_tasks:
        task.start()

    for task in client_tasks:
        task.join()

    out, _ = capsys.readouterr()
    global client_out
    client_out = oh.split_multi_instance_output(out, "C:")


@pytest.mark.order4
def test_integration_can_handle_20_clients__output_correct():
    for key in client_out.keys():
        ass.assert_client_logs_complete(client_out[key])
        ass.assert_calculation_result_is(client_out[key], feedback, execution_out)

    sys.stdout.flush()


@pytest.mark.order5
def test_integration_can_handle_100_clients__run_no_exceptions(capsys):
    client_tasks = []
    for _ in range(100):
        client_tasks.append(r.create_client_task(server_address, code_to_execute))

    for task in client_tasks:
        task.start()

    for task in client_tasks:
        task.join()

    out, _ = capsys.readouterr()
    global client_out
    client_out = oh.split_multi_instance_output(out, "C:")


@pytest.mark.order6
def test_integration_can_handle_100_clients__output_correct():
    for key in client_out.keys():
        ass.assert_client_logs_complete(client_out[key])
        ass.assert_calculation_result_is(client_out[key], feedback, execution_out)

    sys.stdout.flush()


@pytest.mark.order7
def test_integration_can_handle_200_clients__run_no_exceptions(capsys):
    client_tasks = []
    for _ in range(200):
        client_tasks.append(r.create_client_task(server_address, code_to_execute))

    for task in client_tasks:
        task.start()

    for task in client_tasks:
        task.join()

    out, _ = capsys.readouterr()
    global client_out
    client_out = oh.split_multi_instance_output(out, "C:")


@pytest.mark.order8
def test_integration_can_handle_200_clients__output_correct():
    for key in client_out.keys():
        ass.assert_client_logs_complete(client_out[key])
        ass.assert_calculation_result_is(client_out[key], feedback, execution_out)

    sys.stdout.flush()


@pytest.mark.order9
def test_integration_can_handle_500_clients__run_no_exceptions(capsys):
    client_tasks = []
    for _ in range(500):
        client_tasks.append(r.create_client_task(server_address, code_to_execute))

    for task in client_tasks:
        task.start()

    for task in client_tasks:
        task.join()

    out, _ = capsys.readouterr()
    global client_out
    client_out = oh.split_multi_instance_output(out, "C:")


@pytest.mark.order10
def test_integration_can_handle_500_clients__output_correct():
    for key in client_out.keys():
        ass.assert_client_logs_complete(client_out[key])
        ass.assert_calculation_result_is(client_out[key], feedback, execution_out)

    sys.stdout.flush()


@pytest.mark.order11
def test_integration_clean_up():
    r.terminate_server()
    folder = "codes"
    for file in os.listdir(folder):
        filepath = os.path.join(folder, file)
        if os.path.isfile(filepath):
            os.unlink(filepath)

    conn = sqlite3.connect("db/srir.db")
    c = conn.cursor()

    c.execute("DELETE FROM srir;")
    c.execute("DELETE FROM analysis;")
    conn.commit()
    conn.close()

    sys.stdout.flush()

import threading
import asyncio
import Server.src.server as server
import Client.client2 as client
import time
import io
from contextlib import redirect_stdout

loop = None


def run_server():
    global loop
    loop = asyncio.new_event_loop()
    server_task = threading.Thread(target=server.init, args=(loop,))
    server_task.start()
    time.sleep(2)
    return loop


def terminate_server():
    server.terminate(loop)


def run_client_in_this_thread(address, filename):
    data = client.loadFile(filename)
    client.client_run(address, data)


def create_client_task(address, filename):
    data = client.loadFile(filename)
    loop = asyncio.new_event_loop()
    client_task = threading.Thread(target=client.client_run, args=(address, data, loop))
    return client_task

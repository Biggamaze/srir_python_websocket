def split_output(output: str, error: str) -> (list, list, list):
    temp_out = output.split('\n')
    server_output = [x.split(":")[2] for x in temp_out if x and x.startswith("S:")]
    client_output = [x.split(":")[2] for x in temp_out if x and x.startswith("C:")]
    filename = [x.split(":")[1].strip() for x in temp_out if x and x.startswith("Filename:")][0]

    temp_out = error.split('\n')
    error = [x for x in temp_out if x]
    return client_output, server_output, error, filename


def split_multi_instance_output(output: str, line_beginning: str) -> dict:
    temp_out = output.split('\n')
    all_clients_output = [x for x in temp_out if x and x.startswith(line_beginning)]
    client_out_dic = {}

    for line in all_clients_output:
        if line.split(":")[1] in client_out_dic.keys():
            client_out_dic[line.split(":")[1]].append(line.split(":")[2])
        else:
            client_out_dic[line.split(":")[1]] = [line.split(":")[2]]

    return client_out_dic


positive_server_out = ["received complete code",
                       "compiling code",
                       "compiled code",
                       "compilation successful ... executing ...",
                       "execution successful",
                       "storing code",
                       "stored"]

compilation_error_server_out = ["received complete code",
                                "compiling code",
                                "compiled code",
                                "exception during compilation"]


def assert_server_succesfull_logs_complete(server_out: list):
    assert server_out[0:7] == positive_server_out


def assert_server_compilation_error_logs_complete(server_out: list):
    assert server_out[0:4] == compilation_error_server_out


def assert_server_completely_positive_or_negative_compilation(server_out: list):
    assert server_out[0:7] == positive_server_out or server_out[0:4] == compilation_error_server_out


def assert_client_logs_complete(client_out: list):
    assert client_out[0] == "sending number of lines"
    assert client_out[1] == "sending data"
    assert client_out[2] == "awaiting for result"
    assert client_out[-1] == "got result"


def assert_calculation_result_is(client_out: list, feedback: str, exec_result: str):
    assert client_out[-2] == feedback
    assert client_out[-3] == exec_result


def assert_calculation_result_is_either_or(client_out: list, fb1: str, fb2: str, exr1: str, exr2: str):
    assert (client_out[-2] == fb1 and client_out[-3] == exr1) or (client_out[-2] == fb2 and client_out[-3] == exr2)

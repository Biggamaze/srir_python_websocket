import pytest
import IntegrationTesting.helpers.runners as r
import IntegrationTesting.helpers.output_helpers as oh
import IntegrationTesting.helpers.assert_helpers as ass
import os
import sys
import sqlite3

feedback = "No feedback"
execution_out = "Not executed"
server_address = "ws://127.0.0.1:3000/"
code_to_execute = "samplecodes/samplecode2.txt"

server_out = []
client_out = []
error = []
filename = ""


@pytest.mark.order1
def test_integration_can_handle_bad_code_stage_1(capsys):
    r.run_server()
    r.run_client_in_this_thread(server_address, code_to_execute)

    out, err = capsys.readouterr()
    global client_out, server_out, error, filename
    client_out, server_out, error, filename = oh.split_output(out, err)

    r.terminate_server()


@pytest.mark.order2
def test_integration_can_handle_bad_code__check_output__stage_2():
    ass.assert_client_logs_complete(client_out)
    ass.assert_calculation_result_is(client_out, feedback, execution_out)


@pytest.mark.order3
def test_integration__can_handle_bad_code__check_output__stage3():
    ass.assert_server_compilation_error_logs_complete(server_out)


@pytest.mark.order4
def test_integration__can_handle_bad_code__check_error_output__stage4():
    assert error[0] == "No config file found, using default configuration"


@pytest.mark.order5
def test_integration__can_handle_bad_code__check_db__stage5():
    conn = sqlite3.connect("db/srir.db")
    c = conn.cursor()

    c.execute("SELECT num_words, num_lines FROM analysis WHERE guid = (?)", (filename.split(".")[0].split("/")[1],))
    data = c.fetchone()

    assert data is None

    conn.close()


@pytest.mark.order6
def test_clean_up():
    folder = "codes"
    for file in os.listdir(folder):
        filepath = os.path.join(folder, file)
        if os.path.isfile(filepath):
            os.unlink(filepath)

    conn = sqlite3.connect("db/srir.db")
    c = conn.cursor()

    c.execute("DELETE FROM srir;")
    c.execute("DELETE FROM analysis;")
    conn.commit()
    conn.close()

    sys.stdout.flush()
